from tensorflow.keras.models import model_from_json
import numpy as np
# from PIL import Image
        

class VGGFerModel:
    """
    Model for emotions prediction.
    Use image of any size, reshaping it to 48x48 bw.
    Returns name of one of 9 emotions.
    """

    def __init__(self, val_ar=False):
        """
        Load jsons and weights of models on initialize

        :param val_ar: if Valence-Arousal regression is needed
        """
        # flag to show if valence-arousal decomposition should be used
        self.val_ar = val_ar

        # set up paths for model and weights for base classifying model
        model_class_filename = './_models/emotion_classification/model.json'
        weights_filename = './_models/emotion_classification/saved_weights'

        # read json and create model
        with open(model_class_filename, 'r') as json_file:
            loaded_model_class_json = json_file.read()
        self.model_class = model_from_json(loaded_model_class_json)

        # load weights into new model
        self.model_class.load_weights(weights_filename)

        # if model is initialized with val_ar=True:
        if val_ar:
            # set up paths for model and weights for valence-arousal prediction model
            model_val_ar_filename = './_models/valence_arousal_decomposition/model.json'
            weights_val_ar_filename = './_models/valence_arousal_decomposition/saved_weights'

            # read json and create model
            with open(model_val_ar_filename, 'r') as json_file:
                loaded_model_val_ar_json = json_file.read()
            self.model_val_ar = model_from_json(loaded_model_val_ar_json)

            # load weights into new model
            self.model_val_ar.load_weights(weights_val_ar_filename)

        
    def predict(self, value):
        """
        Classify given value

        :param value: array, containing image
        :return: emotion if val_ar==False or list of [emotion, valence, arousal] if val_ar==True
        """
        inp_image = Image.fromarray(value)
        inp_image = inp_image.resize((224, 224))

        # expand dims for input pic
        inp_image = np.expand_dims(inp_image, 0)

        # make and return prediction
        prediction = self.model_class(inp_image)
        num_of_emotion = np.argmax(prediction)
        emotions = ['Anger', 'Contempt',
                    'Disgust', 'Fear',
                    'Happy', 'Neutral',
                    'Sad', 'Surprise', 'Uncertain']
        emotion = emotions[num_of_emotion]
        if not self.val_ar:
            return emotion
        else:
            prediction = self.model_val_ar(inp_image).numpy()
            valence = prediction[0][0]
            arousal = prediction[0][1]
            return [emotion, valence, arousal]
