python .\src\data\raw_train_test_split.py .\data\raw\train.csv .\data\interim\split_train.csv .\data\interim\split_test.csv --n_splits=3
python .\src\data\generate_pairs.py .\data\interim\split_train.csv .\data\interim\split_train_pairs.csv .\reports\metrics_of_split_train_pairs.json --
get_metrics=true
python .\src\features\build_pair_features.py .\data\interim\split_train_pairs.csv .\data\processed\split_train_pair_feautures.csv true
python .\src\models\train_model.py .\models\model_on_split_df.pkl --pair_feautures_dataset_path .\data\processed\split_train_pair_feautures.csv
python .\src\data\generate_pairs.py .\data\interim\split_test.csv .\data\interim\split_test_pairs.csv .\reports\metrics_of_split_test_pairs.json --
get_metrics=true
python .\src\features\build_pair_features.py .\data\interim\split_test_pairs.csv .\data\processed\split_test_pair_feautures.csv true
python .\src\models\predict_model.py .\models\model_on_split_df.pkl .\data\interim\split_test.csv .\data\processed\submission_pred_split.csv --
pair_feautures_dataset_path .\data\processed\split_test_pair_feautures.csv
python .\src\models\evaluate.py .\data\interim\split_test.csv .\data\processed\submission_pred_split.csv .\reports\metrics_of_split_test_final.json